locolab
=======

Local collaboration laboratory

Installation
------------

From the project root directory::

    $ python setup.py install

Usage
-----

Simply run it::

    $ locolab

Use --help/-h to view info on the arguments::

    $ locolab --help

Release Notes
-------------

:0.0.1:
    Project created
