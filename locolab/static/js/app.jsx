(function(){

  var Button = ReactBootstrap.Button;
  var ButtonGroup = ReactBootstrap.ButtonGroup;
  var ButtonToolbar = ReactBootstrap.ButtonToolbar;
  var Table = ReactBootstrap.Table;
  var FormGroup = ReactBootstrap.FormGroup;
  var FormControl = ReactBootstrap.FormControl;
  var ControlLabel = ReactBootstrap.ControlLabel;
  var Checkbox = ReactBootstrap.Checkbox;
  var Nav = ReactBootstrap.Nav;
  var NavItem = ReactBootstrap.NavItem;
  var DropdownButton = ReactBootstrap.DropdownButton;
  var MenuItem = ReactBootstrap.MenuItem;
  var Glyphicon = ReactBootstrap.Glyphicon;
  var sock = io.connect('http://' + document.domain + ':' + location.port + '/sio');
  sock.on('example response', function(msg) {
    console.log('got a response: ' + msg.data);
  });
  sock.emit('example', {data: 'foobar!'}); 
  var BodyMessage = React.createClass({
    getInitialState: function() {
      return {
        path: null,
      };
    },
    render: function() {
      return (
        <div>
          <h1>{this.props.name}</h1>
          <p>Hey there pilgrim!</p>
        </div>
      );
    }
  });

  ReactDOM.render(
    <BodyMessage name="locolab"/>,
    document.getElementById('locoContainer')
  );
})();
