'''
locolab.app
===========
'''
import os
import webbrowser
from base64 import urlsafe_b64encode
from flask import Flask, abort, render_template, send_from_directory
from flask_socketio import SocketIO, emit
app = Flask(__name__, static_url_path='/static')
sio = SocketIO(app)
KEY = None
FILE_DATA = None


@sio.on('example', namespace='/sio')
def test_message(msg):
    print('got test_message: {!r}'.format(msg))
    emit('example response', {'data': 'fizzbuzz'})


@app.route('/static/')
def send_js(path):
    return send_from_directory('/static', path)


@app.route('/<key>')
def index(key):
    if key != KEY:
        abort(403, 'incorrect key')
    return render_template('index.html.j2')


def launch(path=None, host='0.0.0.0', port=8080, key=None):
    global KEY, FILE_DATA
    if key is None:
        KEY = urlsafe_b64encode(os.urandom(24)).decode('utf-8')
    else:
        KEY = key
    with open(path) as f:
        FILE_DATA = f.read()
    webbrowser.open('http://localhost:{port}/{key}'.format(port=port, key=KEY))
    sio.run(app, host=host, port=port)


def main():
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('path')
    parser.add_argument('--host', '-H', default='0.0.0.0')
    parser.add_argument('--port', '-p', type=int, default=8080)
    args = parser.parse_args()
    launch(path=args.path, host=args.host, port=args.port)


if __name__ == '__main__':
    main()
